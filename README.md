RePlan
------

Replan is a simple shell script to edit your linux ".plan" file each day and track
history in git.

The first time you run replan, it will create a directory `~/.plan.d`, perform a `git init` 
and create a file with today's date. It will then add commit that file into git and open it
in vim. Once you exit vim, that file will be added into git and copied to ~/.plan.

Each subsequent time you run git, it will create a file for the day, cat ~/.plan into the end of that file,
and open the file in vim. As you exit vim, it will auto commit the file into vim and copy it over ~/.plan.

## Install
To use, you want to copy the file "replan" into your path somewhere (I have ~/bin in my path).

## Usage

Simply run `replan` at any time to edit your current plan. You can use any format you like to devise your plan
for the day. Your prior plan will be visible for reference. I would recommend copying the relevant bits of your
old plan and deleting the rest. 

If you want to view your plan using finger:

    finger $USER

## Remote Backup

If your plans are not too sensitive, make a private github or gitlab repo and add a cron job to `git push`
on a daily basis. If it's too sensitive, you should host your own git repo on your network.

Example:

    19  22 * * * /usr/bin/git -C ~/.plan.d push gitlab master


